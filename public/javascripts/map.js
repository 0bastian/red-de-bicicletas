var mymap = L.map('mapid').setView([-32.9413724, -60.675548], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);



// L.marker([-32.9413724, -60.675548]).addTo(mymap)
//     .bindPopup('Bienvenido a Rosario.</br><img src="https://png.pngtree.com/png-clipart/20190920/original/pngtree-cute-little-bee-illustration-png-image_4636934.jpg" width=100px heigth=100px>.')
//     .openPopup();


$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result) {
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(mymap);
        });
    }
})