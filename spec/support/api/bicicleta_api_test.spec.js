var Bicicleta = require('../../../models/bicicleta');
var request = require('request');
var server = require('../.././../bin/www');

beforeEach(() => { console.log("´testeando...´"); })
    // beforeAll(() => { console.log("testeando") });
beforeEach(() => { Bicicleta.allBicis = []; });


describe('Bicicleta API', () => {
    describe('GET Bicicletas /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(3, "Azul", "MTB", [-32.9476745, -60.6520952]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body) {
                expect(response.statusCode).toBe(200);
            });


        })
    })
})


describe(' POST Bicicleta /create', () => {
    it('Status 200', (done) => {
        expect(Bicicleta.allBicis.length).toBe(0);
        let headers = { 'content-type': 'application/json' }

        let a = {
            "id": 3,
            "color": "Azul",
            "modelo": "MTB",
            "lat": -32,
            "lng": -60
        };

        let bici = JSON.stringify(a);


        //var bici = '{"id":3, "color":"Azul","modelo":"MTB","lat":-32 ,"lng":-60}';



        request.post({
            headers: headers,
            url: 'http://localhost:3000/api/bicicletas/create',
            body: bici
        }, function(error, response, body) {
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(3).color).toBe("Azul");
            done();
        });
    })
})