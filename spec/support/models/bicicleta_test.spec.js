var Bicicleta = require('../../../models/bicicleta');


beforeEach(() => { Bicicleta.allBicis = []; });


describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    })
})

describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        let a = new Bicicleta(1, "Rojo", "Urbana", [-32.9522582, -60.6685305]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    })
})


describe('Bicicletas.findById', () => {
    it('debe volver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        let bici = new Bicicleta(1, "Blanca", "BMX", [-32.946297, -60.6556573]);
        let bici2 = new Bicicleta(2, "Azul", "MTB", [-32.9476745, -60.6520952]);
        Bicicleta.add(bici);
        Bicicleta.add(bici2);

        var targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe("Blanca");
        expect(targetBici.modelo).toBe("BMX");
    })
})


describe('Bicicletas.removeById', () => {
    it('debe borrar la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        let bici = new Bicicleta(1, "Blanca", "BMX", [-32.946297, -60.6556573]);
        Bicicleta.add(bici);
        expect(Bicicleta.allBicis.length).toBe(1);
        var targetBici = Bicicleta.removeById(bici.id);
        expect(Bicicleta.allBicis.length).toBe(0);
    })
})